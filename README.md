# Goldmine migration

This project is a basic script that parses the XML output of Goldmine contacts and converts it to a csv format for import into FreshSales and Zoho CRM software.

# Usage 

Export the contacts in Goldmine via File->Export as XML. Select the appropriate history.

# Acknowledgements

This work was funded by [National Wireless Pty Ltd](https://www.nationalwireless.com.au/) for internal use and provided without warranty in the hope it will be useful to others.
