import html2text
import email
import pprint


from email import policy


def _properties(field, contact, notes):
    """Process the properties tag as they came along"""

    if "name" not in field.attrib:
        return

    key = field.attrib["name"]

    if key not in contact:
        print(f"Key {key} not found! Please add into dictionary")

    property_string = field.find("property_string")

    if property_string is None:
        return

    text = property_string.text

    if key == "Notes" and text is not None:

        notes.append(
            {
                "Note title": "Imported from Goldmine {}".format(
                    field.attrib["modified"]
                ),
                "Note Content": html2text.html2text(text),
                "Parent name": contact["Company"],
                "Parent ID": contact["Account No"],
                "Created By": field.attrib["modified_by"],
            }
        )
    else:
        contact[key] = text if key != "State" else text.upper()


def properties(account, contact, notes):
    """Parse the properties within the xml element"""
    for field in account:
        _properties(field, contact, notes)


def create_additional_contact_dict():
    """Goldmine additional contacts"""
    return {
        key: ""
        for key in (
            "address1",
            "address2",
            "address3",
            "city",
            "contact_name",
            "contact_title",
            "country",
            "dear",
            "email_address",
            "phone_number",
            "notes",
            "reference",
            "state_county",
            "zip_postalcode",
            # Added to convert these additional contacts into a contact
            "Account No",
            "Company",
            "Record Type",
        )
    }


def additional_contacts(account, contact, additional_contacts):

    unfound_keys = []

    for fields in account:

        additional_contact = create_additional_contact_dict() | {
            "Company": contact["Company"],
            "Account No": contact["Account No"],
            "Record Type": "",
            "Mail List": "",
        }

        for field in fields:
            if field.tag == "properties":
                for entry in field:
                    if "name" not in entry.attrib:
                        continue

                    name_key = entry.attrib["name"]

                    if name_key not in additional_contact:
                        unfound_keys.append(name_key)

                    additional_contact[name_key] = entry.find("property_string").text

            elif field.tag == "emails":
                emails(field, additional_contact)
            elif field.tag == "phone_numbers":
                phone_numbers(field, additional_contact)

        additional_contacts.append(additional_contact)

    if unfound_keys:
        print("Unfound keys:")
        pprint.pprint(unfound_keys)
        quit()


def emails(account, contact, secondary_emails=None):
    for fields in account:

        is_primary = (
            fields.attrib["primary"] == "1" if "primary" in fields.attrib else None
        )

        for field in fields:
            for email in field:

                if (
                    "name" not in email.attrib
                    or email.attrib["name"] != "email_address"
                ):
                    continue

                property_string = email.find("property_string")

                if property_string is None:
                    continue

                k = email.attrib["name"]

                # Lower case the email address. Technically they could refer
                # to a different inbox *but* it was likely user error that
                # entered with camel case or something else to avoid
                # deduplication. We'll take the small risk that it's not case
                # sensitive.
                email_address = property_string.text.lower()

                if is_primary is None or is_primary:
                    contact[k] = email_address
                else:
                    secondary_emails[email_address] = contact["Company"]


def phone_numbers(account, contact):

    key = "phone_number"

    contact[key] = []

    for fields in account:
        type = fields.attrib["type"]
        for field in fields:
            for phone in field:
                if phone.attrib["name"] == key:

                    contact[key].append(
                        (
                            type,
                            phone.find("property_string").text,
                        )
                    )


def websites(account, contact):
    for website in account:
        for website_data in website:
            for website_name in website_data:

                if "name" not in website_name.attrib:
                    continue

                key = website_name.attrib["name"]

                if key != "web_site":
                    continue

                property_string = website_name.find("property_string")

                if not contact[key]:
                    contact[key] = property_string.text
                else:
                    contact[key] += " " + property_string.text


def _history(history_item, account, contact, additional_contacts, notes):

    item_type = history_item.attrib["type"].replace("_", " ")

    for history_data in history_item:

        notes.append(
            {
                "Note Content": "",
                "Note Title": "",
                "Parent Name": contact["Contact"],
                "Parent ID": contact["email_address"],
            }
        )

        notes[-1]["Parent ID"] = contact["email_address"]

        for name in history_data:

            if "name" not in name.attrib or name is None:
                continue

            attrib_name = name.attrib["name"]

            if attrib_name == "notes":

                notes[-1]["Note Content"] = html2text.html2text(
                    name.find("property_string").text
                )

            elif attrib_name == "completed_on":

                notes[-1]["Note Title"] = "Goldmine {} {}".format(
                    item_type, name.find("property_datetime").text
                )

            elif attrib_name == "linked_to":

                linked_name = name.find("property_string").text

                assert linked_name is not None

                def update_link(email, name):
                    return {
                        "Parent ID": email,
                        "Parent Name": name,
                    }

                if contact["Contact"] == linked_name:
                    notes[-1].update(
                        update_link(contact["email_address"], contact["Contact"])
                    )
                    break

                # Find all the matching additional contacts
                matching_contacts = [
                    additional_contact
                    for additional_contact in additional_contacts
                    if additional_contact["contact_name"] == linked_name
                ]

                if not matching_contacts:
                    break

                # If nobody has a valid email address we can't do a link
                # back to the contact, so better to forget about this mess
                if not all([contact["email_address"] for contact in matching_contacts]):
                    # print(
                    #     " " * 2,
                    #     "The linked contacts did not list an email address.",
                    # )
                    # pprint.pprint(matching_contacts)
                    del notes[-1]
                    return

                for additional_contact in matching_contacts:
                    notes[-1].update(
                        update_link(
                            additional_contact["email_address"],
                            additional_contact["contact_name"],
                        )
                    )


def history(account, contact, additional_contacts, notes):
    for history_item in account:
        _history(history_item, account, contact, additional_contacts, notes)


def messages(account, contact, messages):
    for message_item in account:
        for message_data in message_item:
            for message_name in message_data:

                if (
                    "name" not in message_name.attrib
                    or message_name.attrib["name"] != "rfc822"
                ):
                    continue

                # Parse an email in RFC 822 format (deprecated)
                # There are some encoding mismatches from Goldmine
                # so we just have to handle these as best we can
                message = email.message_from_string(
                    message_name.find("property_string").text,
                    policy=policy.SMTPUTF8,
                )

                if message:
                    messages.append(message)
