import argparse
import csv
import json
import xml.etree.ElementTree as ET
import imap_tools
import time
import re

import cProfile
import pstats
from pstats import SortKey

import goldmine_parser as parser

from imap_tools import MailBox

from fuzzywuzzy import fuzz

from tqdm import tqdm

index_print = 0


def create_goldmine_dict():
    """Goldmine export fields"""
    return {
        key: ""
        for key in (
            "Acc Mgr",
            "Account No",
            "Address",
            "Callbackat",
            "Callbackon",
            "Callbkfreq",
            "Closedate",
            "Company",
            "Contact",
            "Comments",
            "Country",
            "Createat",
            "Createby",
            "Createon",
            "Dear",
            "Dept",
            "email_address",
            "Industry",
            "Interest",
            "Lastatmpat",
            "Lastatmpon",
            "Lastcontat",
            "Lastconton",
            "Lastdate",
            "Lastname",
            "Lasttime",
            "Lastuser",
            "Location",
            "Mail List",
            "Meetdateon",
            "Meettimeat",
            "Nextaction",
            "Notes",
            "Pcode",
            "phone_number",
            "Prevresult",
            "Record Type",
            "Source",
            "State",
            "Suburb",
            "Territory",
            "Title",
            "Secr",
            "Userdef01",
            "Userdef02",
            "Userdef03",
            "Userdef04",
            "Userdef05",
            "Userdef06",
            "Userdef07",
            "Userdef08",
            "Userdef09",
            "web_site",
            "Actionon",
            "-",
        )
    } | {"Notes": []}


def create_zoho_lead_dict():
    """Fields for a Zoho lead"""
    return {
        key: ""
        for key in (
            "Lead ID",
            "Lead Owner",
            "Lead Owner ID",
            "Company",
            "First Name",
            "Last Name",
            "Title",
            "Email",
            "Phone",
            "Fax",
            "Mobile",
            "Website",
            "Lead Source",
            "Lead Status",
            "Industry",
            "No. of Employees",
            "Annual Revenue",
            "Rating",
            "Email Opt Out",
            "Created By",
            "Created by ID",
            "Modified By",
            "Modified by ID",
            "Created Time",
            "Modified Time",
            "Salutation",
            "Secondary Email",
            "Last Activity Time",
            "Tag",
            "Prediction",
            "Street",
            "City",
            "State",
            "Country",
            "Description",
            "Lead Conversion Time",
            "Unsubscribed Mode",
            "Unsubscribed Time",
            "Converted Account",
            "Converted Account Id",
            "Converted Contact",
            "Converted Contact Id",
            "Converted Deal",
            "Converted Deal Id",
            "Postcode",
        )
    }


def create_zoho_contact_dict():
    """Zoho required entries"""
    return {
        key: ""
        for key in (
            "Contact ID",
            "Contact Owner",
            "Contact Owner ID",
            "Lead Source",
            "First Name",
            "Last Name",
            "Account Name",
            "Account ID",
            "Vendor Name",
            "Vendor ID",
            "Email",
            "Title",
            "Department",
            "Phone",
            "Home Phone",
            "Other Phone",
            "Fax",
            "Mobile",
            "Date of Birth",
            "Assistant",
            "Asst Phone",
            "Reporting To",
            "Reporting To Id",
            "Email Opt Out",
            "Skype ID",
            "Created By",
            "Created by ID",
            "Modified By",
            "Modified by ID",
            "Created Time",
            "Modified Time",
            "Salutation",
            "Secondary Email",
            "Last Activity Time",
            "Twitter",
            "Tag",
            "Mailing Street",
            "Other Street",
            "Mailing City",
            "Other City",
            "Mailing State",
            "Other State",
            "Mailing Zip",
            "Other Zip",
            "Mailing Country",
            "Other Country",
            "Description",
            "Unsubscribed Mode",
            "Unsubscribed Time",
            "Territory",
        )
    }


def create_zoho_account_dict():
    return {
        k: ""
        for k in (
            "Account ID",
            "Account Owner",
            "Account Owner ID",
            "Rating",
            "Account Name",
            "Phone",
            "Parent Account",
            "Parent Account ID",
            "Website",
            "Ticker Symbol",
            "Account Type",
            "Ownership",
            "Industry",
            "Annual Revenue",
            "SIC Code",
            "Created By",
            "Created by ID",
            "Modified By",
            "Modified by ID",
            "Created Time",
            "Modified Time",
            "Last Activity Time",
            "Account Number",
            "Account Site",
            "Tag",
            "Billing Street",
            "Shipping Street",
            "Billing City",
            "Shipping City",
            "Billing State",
            "Shipping State",
            "Billing Code",
            "Shipping Code",
            "Billing Country",
            "Shipping Country",
            "Description",
        )
    }


def goldmine_to_zoho_contact_key_map():
    """Goldmine to Zoho map"""
    return (
        ("Account No", "Pronto Account"),
        ("Address", "Mailing Street"),
        ("Company", "Account Name"),
        ("Country", "Mailing Country"),
        ("Createby", "Created By"),
        ("Createat", "Created Time"),
        ("email_address", "Email"),
        ("Lastuser", "Modified By"),
        ("Pcode", "Mailing Zip"),
        ("Record Type", "Account Type"),
        ("State", "Mailing State"),
        ("Suburb", "Mailing City"),
        ("Source", "Lead Source"),
        ("Lasttime", "Modified Time"),
        ("Dept", "Department"),
    )


def goldmine_to_zoho_additional_contact_key_map():
    return (
        ("Account No", "Pronto Account"),
        ("address1", "Mailing Street"),
        ("Company", "Account Name"),
        ("country", "Mailing Country"),
        ("contact_title", "Title"),
        ("email_address", "Email"),
        ("zip_postalcode", "Mailing Zip"),
        ("state_county", "Mailing State"),
        ("city", "Mailing City"),
        ("reference", "Department"),
        ("Record Type", "Account Type"),
    )


def write_to_csv(file_name, output_data):
    """Dump out the dictionary into a csv ready for importing"""

    if not output_data:
        return

    with open(file_name, "w", newline="") as csvfile:
        writer = csv.writer(csvfile, delimiter=",", quoting=csv.QUOTE_MINIMAL)

        writer.writerow(output_data[0].keys())

        for entry in output_data:
            writer.writerow(entry.values())


def generate_verification_emails(contacts, additional_contacts, secondary_emails):

    contact_emails = {
        contact["email_address"] for contact in contacts if contact["email_address"]
    }

    additional_contact_emails = {
        additional_contact["email_address"]
        for additional_contact in additional_contacts
        if additional_contact["email_address"]
    }

    secondary_emails = {
        k: v
        for k, v in secondary_emails.items()
        if all(
            k not in emails for emails in (additional_contact_emails, contact_emails)
        )
    }

    def generate_name_dict(keys, values):
        return {k: v for k, v in zip(keys, split_contact_name(values))}

    write_to_csv(
        "secondary_emails.csv",
        [
            {
                "email": k,
                "company": v,
            }
            for k, v in secondary_emails.items()
            if "@" in k
        ],
    )
    write_to_csv(
        "additional_contact_emails.csv",
        [
            {
                "email": additional_contact["email_address"],
                "company": additional_contact["Company"],
                **generate_name_dict(
                    ("first name", "last name"), additional_contact["contact_name"]
                ),
            }
            for additional_contact in additional_contacts
            if additional_contact["email_address"] in additional_contact_emails
            and "@" in additional_contact["email_address"]
            and additional_contact["contact_name"]
        ],
    )


def split_contact_name(name):
    """Splits a tuple of the first and last names"""
    first_name, *last_names = name.split()
    return first_name, " ".join(last_names)


def is_mobile_number(number):
    return number.find("04", 0, 2) != -1


def format_phone_number(phone_numbers):

    contact = {}

    for type, phone_number in phone_numbers:

        type = type.lower()

        # Strip leading and trailing whitespace
        # Replace dashes with whitespace
        number = phone_number.strip().replace("-", " ")

        if number[0] != "0" and number.find("1300", 0, 4) == -1:
            number = "".join(("0", number))

        if type == "mobile" or is_mobile_number(number):
            contact["Mobile"] = number
        elif type in ("phone", "office"):
            contact["Phone"] = number

    return contact


def populate_zoho_contact_record(goldmine_contact, map_function):

    contact = create_zoho_contact_dict()

    contact_key = "Contact" if "Contact" in goldmine_contact else "contact_name"

    if goldmine_contact[contact_key]:
        contact["First Name"], contact["Last Name"] = split_contact_name(
            goldmine_contact[contact_key]
        )

    if goldmine_contact["phone_number"]:
        contact.update(format_phone_number(goldmine_contact["phone_number"]))

    # Direct matches copy
    contact.update(
        {key: goldmine_contact[key] for key in goldmine_contact if key in contact}
    )

    # Mapped copy
    contact.update(
        {zoho_key: goldmine_contact[gm_key] for gm_key, zoho_key in map_function()}
    )

    if "Mail List" in goldmine_contact:
        contact["Email Opt Out"] = goldmine_contact[
            "Mail List"
        ] is None or goldmine_contact["Mail List"].lower() not in ("yes")

    return contact


def export_to_zoho(
    goldmine_contacts,
    additional_contacts,
    account_counts,
    account_notes,
    contact_notes,
    secondary_emails,
):
    """Convert Goldmine data to Zoho and write to a csv file"""

    write_to_csv("zoho_account_notes.csv", account_notes)
    write_to_csv("zoho_contact_notes.csv", contact_notes)
    write_to_csv(
        "zoho_lead_import.csv",
        [
            {"Lead Source": "Goldmine Import", "Email": email, "Company": company}
            for email, company in secondary_emails.items()
        ],
    )

    contacts, accounts = [], {}

    for goldmine_contact in goldmine_contacts:

        # Scrap the account information from the goldmine contacts / account
        # records and use this to populate Zoho

        if (
            goldmine_contact["Account No"] not in account_counts
            and not goldmine_contact["Company"]
        ):
            continue

        key = (
            goldmine_contact["Account No"]
            if goldmine_contact["Account No"]
            else goldmine_contact["Company"]
        )

        # There is doubling up of data here but it makes the csv simplier
        accounts[key] = create_zoho_account_dict() | {
            "Account Name": goldmine_contact["Company"],
            "Account Type": goldmine_contact["Record Type"],
            "Industry": goldmine_contact["Industry"],
            "Pronto Account": key if goldmine_contact["Account No"] else "",
            "Website": set(goldmine_contact["web_site"].split()).pop()
            if goldmine_contact["web_site"]
            else "",
        }

    write_to_csv("zoho_account_import.csv", list(accounts.values()))

    contacts = [
        populate_zoho_contact_record(contact, goldmine_to_zoho_contact_key_map)
        for contact in goldmine_contacts
    ] + [
        populate_zoho_contact_record(
            contact, goldmine_to_zoho_additional_contact_key_map
        )
        for contact in additional_contacts
        if contact["email_address"]
    ]

    write_to_csv("zoho_contact_import.csv", contacts)


def process_account(
    contact,
    additional_contacts,
    messages,
    account_notes,
    contact_notes,
    account,
    secondary_emails,
):
    """Process the xml tags as they arrive from Goldmine"""

    if account.tag == "properties":
        parser.properties(account, contact, account_notes)
    elif account.tag == "addl_contacts":
        parser.additional_contacts(account, contact, additional_contacts)
    elif account.tag == "emails":
        parser.emails(account, contact, secondary_emails)
    elif account.tag == "phone_numbers":
        parser.phone_numbers(account, contact)
    elif account.tag == "websites":
        parser.websites(account, contact)
    elif account.tag == "history":
        parser.history(account, contact, additional_contacts, contact_notes)
    elif account.tag == "messages":
        parser.messages(account, contact, messages)


def extract_elements(context, tag_name: str):
    """Generator to extract xml elements on the fly to reduce memory usage"""
    for event, element in context:
        if event == "end" and element.tag == tag_name:
            yield element
            element.clear()


def merge_duplicates(contacts, threshold=70, partial_threshold=70):
    """Detects possible duplicates in the contact name across contacts"""

    def is_partial_match(name_0, name_1):
        return (
            fuzz.partial_ratio(name_0, name_1) > partial_threshold
            and fuzz.ratio(name_0, name_1) > threshold
        )

    def is_duplicate(contact, d_contact, index, d_index, set0, set1):
        return (
            d_index + index + 1 not in set0
            and d_index + index + 1 not in set1
            and is_partial_match(name, d_contact["contact_name"])
        )

    contacts = [
        contact for contact in contacts if contact["reference"] != "Duplicate Record"
    ]

    duplicate_indices = set()
    skipped_indices = set()

    duplicate_contact_indices = []

    # Find any fuzzy matches and merge these based on which information is better
    for index, contact in enumerate(contacts):

        name = contact["contact_name"]

        if not name:
            contact["contact_name"] = contact["contact_title"]
            skipped_indices.add(index)
            continue

        forward_indices = [
            d_index + index + 1
            for d_index, d_contact in enumerate(contacts[index + 1 :])
            if is_duplicate(
                contact, d_contact, index, d_index, skipped_indices, duplicate_indices
            )
        ]

        if not forward_indices:
            continue

        duplicate_indices |= set([*forward_indices, index])
        duplicate_contact_indices.append([*forward_indices, index])

    duplicate_contacts = [
        [contacts[i] for i in d_c_i] for d_c_i in duplicate_contact_indices
    ]

    if len(duplicate_indices) > 0:
        print(f"\n  Merged {len(duplicate_indices)} duplicate contacts")

    # Remove all the contacts that are duplicated
    contacts = [
        contact for i, contact in enumerate(contacts) if i not in duplicate_indices
    ]

    for duplicate_contact in duplicate_contacts:

        merged_contact = duplicate_contact[0]

        for contact in duplicate_contact[1:]:

            merged_contact.update(
                {k: v for k, v in contact.items() if v and not merged_contact[k]}
            )

            # Update fields where bigger is better
            for k in ("contact_name", "contact_title"):
                if len(contact[k]) > len(merged_contact[k]):
                    merged_contact[k] = contact[k]

            for k, v in contact["phone_number"]:
                if k not in [type for type, number in merged_contact["phone_number"]]:
                    merged_contact["phone_number"].append((k, v))


def remove_duplicate_secondary(secondary_emails, contact, additional_contacts_local):

    if contact["email_address"] in secondary_emails:
        del secondary_emails[contact["email_address"]]

    for additional_contact in additional_contacts_local:

        email_address = additional_contact["email_address"]

        if email_address in secondary_emails:
            del secondary_emails[email_address]


def export_emails_to_imap(emails):
    """Append the emails into the specified IMAP server
    so the CRM can find the relevant emails
    """

    with open("config.json", "r") as json_file:
        config = json.load(json_file)

    with open("valid_email_addresses.json", "r") as json_file:
        email_json = json.load(json_file)

    with MailBox(config["server"]).login(
        config["username"], config["password"], initial_folder="INBOX"
    ) as mailbox:

        mail_folder = "INBOX/Goldmine"

        if not mailbox.folder.exists(mail_folder):
            mailbox.folder.create(mail_folder)

        failed_emails = []

        valid_email_addresses = set(email_json["emails"])

        # Extract what is between the angled brackets for parsing emails
        r = re.compile(r"<(.*?)>")

        print("Transferring emails to IMAP server")

        for email in tqdm(emails):

            try:

                if email["From"] is None or email["To"] is None:
                    # Something is fishy with the email so ignore
                    continue

                from_address = (
                    r.search(email["From"]).group(1)
                    if "<" in email["From"]
                    else email["From"]
                )

                to_address = (
                    r.search(email["To"]).group(1)
                    if "<" in email["To"]
                    else email["To"]
                )

                if (
                    from_address not in valid_email_addresses
                    and to_address not in valid_email_addresses
                ):
                    email.replace_header(
                        "From" if config["email_substring"] in email["From"] else "To",
                        config["spoof_email"],
                    )

                mailbox.append(
                    email.as_bytes(),
                    "INBOX/Goldmine",
                    dt=None,
                    flag_set=(imap_tools.MailMessageFlags.SEEN),
                )
            except KeyError:
                print(" " * 2, "Invalid encoding found")
                failed_emails.append(email)

    print(
        "A total of {} sent emails and {} failed emails".format(
            len(failed_emails) - len(failed_emails), len(failed_emails)
        )
    )


def parse_goldmine_output(file_name: str):
    """Parse the xml output of Goldmine"""

    print("Parsing Goldmine data")

    parser_start = time.time()

    with open(file_name, "r") as xml_file:

        context = iter(ET.iterparse(xml_file, events=("start", "end")))

        event, root = next(context)

        if root.tag != "gmdata":
            print("The xml file does not appear to come from Gold Mine")
            quit()

        contacts = []
        emails = []

        additional_contacts = []
        account_notes = []
        contact_notes = []

        secondary_emails = {}

        for account in extract_elements(context, "account"):

            print(f"Processing contact {len(contacts) + 1}", end="")

            start = time.time()

            additional_contacts_local = []
            account_notes_local = []
            contact_notes_local = []

            contact = create_goldmine_dict()

            for element in account:
                process_account(
                    contact,
                    additional_contacts_local,
                    emails,
                    account_notes_local,
                    contact_notes_local,
                    element,
                    secondary_emails,
                )

            merge_duplicates(additional_contacts_local)

            remove_duplicate_secondary(
                secondary_emails, contact, additional_contacts_local
            )

            contacts.append(contact)

            additional_contacts.extend(additional_contacts_local)
            account_notes.extend(account_notes_local)
            contact_notes.extend(contact_notes_local)

            print(
                " ({}) took {:.2f}s".format(
                    contacts[-1]["Contact"], time.time() - start
                )
            )

        if emails:
            print(f"  Found a total of {len(emails)} emails")
        if account_notes:
            print(f"  Found a total of {len(account_notes)} account notes")
        if contact_notes:
            print(f"  Found a total of {len(contact_notes)} contact notes")
        if contacts:
            print(f"  Found a total of {len(contacts)} contacts")
        if additional_contacts:
            print(f"  Found a total of {len(additional_contacts)} additional contacts")

    print("Parsing Goldmine took {:.2f} seconds".format(time.time() - parser_start))

    return (
        contacts,
        additional_contacts,
        account_notes,
        contact_notes,
        emails,
        secondary_emails,
    )


def create_goldmine_account_count(contacts):
    """Count the number of instances for an account"""

    accounts = {name: 0 for name in {contact["Account No"] for contact in contacts}}

    for contact in contacts:
        accounts[contact["Account No"]] += 1

    accounts.pop("", None)

    return accounts


def zombify_contact_records(additional_contacts, file_name):

    with open(file_name, newline="") as csv_file:
        zombie_emails = {row[0] for row in csv.reader(csv_file)}

        for contact in additional_contacts:
            if contact["email_address"] in zombie_emails:
                contact["Record Type"] = "Zombie"


def remove_invalid_secondary_emails(secondary_emails, file_name):
    with open(file_name, newline="") as csv_file:
        for row in csv.reader(csv_file):
            if row[0] in secondary_emails:
                del secondary_emails[row[0]]


if __name__ == "__main__":

    cmd_parser = argparse.ArgumentParser(
        description="Convert Goldmine xml database to csv for importing into Zoho CRM"
    )

    cmd_parser.add_argument("filename", help="Filename of Goldmine xml output")

    cmd_parser.add_argument(
        "--export-emails",
        dest="export_emails",
        help="Output email addresses of additional and secondary contacts to csv",
        action="store_true",
    )

    cmd_parser.add_argument(
        "--export-to-imap",
        dest="export_to_imap",
        help="Export emails to IMAP folder defined by config.json",
        action="store_true",
    )

    cmd_parser.add_argument(
        "--profile",
        help="Advanced: enable profile metrics",
        action="store_true",
    )

    cmd_parser.add_argument(
        "--bounced-secondary-emails",
        dest="bounced_secondary_emails",
        type=str,
        help="Path to csv of bounced secondary email addresses",
    )

    cmd_parser.add_argument(
        "--bounced-additional-emails",
        dest="bounced_additional_emails",
        type=str,
        help="Path to csv of bounced additional contact emails (inactive but needed for history)",
    )

    args = cmd_parser.parse_args()

    enable_profile = args.profile

    if enable_profile:
        profile = cProfile.Profile()
        profile.enable()

    (
        contacts,
        additional_contacts,
        account_notes,
        contact_notes,
        emails,
        secondary_emails,
    ) = parse_goldmine_output(args.filename)

    if args.export_emails:
        generate_verification_emails(contacts, additional_contacts, secondary_emails)

    if args.bounced_additional_emails is not None:
        zombify_contact_records(additional_contacts, args.bounced_additional_emails)

    if args.bounced_secondary_emails is not None:
        previous_len = len(secondary_emails)
        remove_invalid_secondary_emails(secondary_emails, args.bounced_secondary_emails)
        print(
            f"Removed {previous_len - len(secondary_emails)} invalid secondary email(s)"
        )

    if enable_profile:
        profile.disable()
        statistics = pstats.Stats(profile).sort_stats(SortKey.CUMULATIVE)
        statistics.print_stats()

    if args.export_to_imap:
        export_emails_to_imap(emails)

    export_to_zoho(
        contacts,
        additional_contacts,
        create_goldmine_account_count(contacts),
        account_notes,
        contact_notes,
        secondary_emails,
    )
